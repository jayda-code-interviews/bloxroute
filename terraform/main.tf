provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
}
### Update vpc and subnet ids ###
locals {
  vpc_id    = "vpc-dbd208a0"
  subnet_id = "subnet-1ff21155"
  ecr_repo  = jsondecode(file("${path.module}/../repo-info.json"))["repository"]["repositoryUri"]
}

output "ecr_repo" {
  value = local.ecr_repo
}

output "ip_addrs" {
  value = module.ec2_cluster.public_ip
}

resource "aws_iam_role" "ec2_role" {
  name = "jayda_interview"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    owner = "jayda"
  }
}

resource "aws_iam_instance_profile" "ecr_profile" {
  name = "jayda_interview"
  role = aws_iam_role.ec2_role.name
}

resource "aws_iam_role_policy" "test_policy" {
  name = "test_policy"
  role = aws_iam_role.ec2_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1562909381257",
      "Action": [
        "ecr:BatchCheckLayerAvailability",
        "ecr:BatchGetImage",
        "ecr:DescribeImages",
        "ecr:DescribeRepositories",
        "ecr:GetAuthorizationToken",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetLifecyclePolicy",
        "ecr:GetLifecyclePolicyPreview",
        "ecr:GetRepositoryPolicy",
        "ecr:ListImages",
        "ecr:ListTagsForResource"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allows http traffic"

  vpc_id = local.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Owner = "jayda"
  }
}

module "ec2_cluster" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "jayda-interview"
  instance_count = 2

  ami           = "ami-02507631a9f7bc956"
  instance_type = "t2.micro"

  vpc_security_group_ids = [aws_security_group.allow_http.id]
  subnet_id              = local.subnet_id
  iam_instance_profile   = aws_iam_instance_profile.ecr_profile.name

  associate_public_ip_address = "true"

  user_data = templatefile("${path.module}/user_data.tmpl",
    {
      ecr_repo = local.ecr_repo
  })

  tags = {
    Owner = "jayda"
  }
}
