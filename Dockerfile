FROM python:2.7

# Create app directory
WORKDIR /app

# Bundle app source
COPY app /app

EXPOSE 8080
CMD [ "python", "./server.py" ]
