# bloXroute

### Prerequisites

* [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)
**version 12 or higher**
* awscli
* jq
* Docker

### Configuration

The script assumes that there is already a VPC and Public Subnet available to
run in. The `vpc_id` and `subnet_id` inside of `terraform/main.tf` will need to
be edited to reflect what's available.

### Running the script

`./deploy.sh`

### Cleanup
* From the terraform directory run `terraform destroy`
* The ECR repo and images will need to be manually deleted
* Most of the resources that get created are have the tag `owner: jayda` so they
should be easy to find if for some reason Terraform doesn't remove everything

### Note

This runs with default AWS system creds. You can change these in `$HOME/.aws/`
or by using AWS envars. https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

