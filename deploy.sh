#!/bin/bash

region='us-east-1'
repo_name='bloxroute'
time_out=300

$(aws ecr get-login --no-include-email --region ${region})

## Create ECR Repository
if [ ! -f $PWD/repo-info.json ]; then
	echo "Creating ECR repository"
	aws ecr create-repository --repository-name ${repo_name} --output json | tee repo-info.json
fi

## Upload image to ECR
docker build -t ${repo_name} . 
docker tag ${repo_name}:latest $(cat repo-info.json | jq '.repository.repositoryName' |  tr -d \"):latest
docker push $(cat repo-info.json | jq '.repository.repositoryUri' |  tr -d \")

## Apply Terraform
cd terraform
terraform init
terraform apply

## Check Servers
for ip in $(terraform output -json | jq -r '.ip_addrs.value[]'); do
	SECONDS=0
	while [ $SECONDS -lt $time_out ] ; do
		if [ $(curl -sL -w "%{http_code}\\n" "http://$ip" -o /dev/null) == "200" ]; then
			echo "$ip is healthy"
			break
		fi
	done

	if [ $SECONDS -gt $time_out ]; then
		echo "$ip timed out"
	fi
done
